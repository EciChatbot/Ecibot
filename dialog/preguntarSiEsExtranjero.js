module.exports = {
	getDialogo:function(parametros) { 		var builder=parametros._builder; 		var ecibot=parametros._ecibot; 		var request=parametros._request;
		return [
			/**
			 * pregunta
			 */
			function (session, args, next) {
				builder.Prompts.confirm(session, "¿Eres estudiante extranjero?");
			},
			/**
			 * retorna la respuesta del usuario
			 */
			function (session, results) {
				session.endDialogWithResult(results);
			}
		]
	}
}