module.exports = {
	getDialogo:function(parametros) {
		var builder=parametros._builder;
		var ecibot=parametros._ecibot;
		var request=parametros._request;
		return [
			/**
			 * pide con opciones el tipo de ayuda que necesita sobre Admisiones
			 */
			function (session, args, next) {
				builder.Prompts.choice(session, "Con mucho gusto, pero específicamente en qué necesitas ayuda de tu "+session.userData.nivelGrado+":", "Requisitos|documentos|Pasos|Respuesta", { listStyle: 4 });
			},
			/**
			 * retorna la respuesta y la guarda en userData
			 */
			function (session, results) {
				session.userData.AdmInfo = results.response.entity;
				session.endDialogWithResult(results);
			}
		]
	}
}