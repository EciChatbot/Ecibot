module.exports = {
	getDialogo:function(parametros) { 		var builder=parametros._builder; 		var ecibot=parametros._ecibot; 		var request=parametros._request;
		return [
			/**
			 * si no tiene el nivel de grado lo pregunta
			 */
			function (session, args, next) {
				if (!session.userData.nivelGrado)
					session.beginDialog("pedirNivelGrado");
				else next();
			},
			/**
			 * si no tiene la información que desea el usuario sobre admisiones lo pregunta
			 */
			function (session, results, next) {
				if (!session.userData.AdmInfo)
					session.beginDialog("pedirInfoGuiaAdmisiones");
				else next();
			},
			/**
			 * inicia el diálogo correspondiente para cada ayuda que requiera el usuario sobre Admisiones
			 */
			function (session, results) {
				
				switch (session.userData.AdmInfo.toLowerCase()) {
					case "requisitos":
						session.userData.revisoRequisitosAdmision=true;
						session.beginDialog("guiaAdmisionesRequisitos");
						break;
					case "pasos":
					case "paso":
					case "procedimiento":
						if(!session.userData.revisoRequisitosAdmision){
							session.send("Te recomendamos que revises los requisitos de admisión antes de empezar, si deseas revisarlos puedes escribirme: \"**Requisitos de admisión**\"");
						}
						session.beginDialog("guiaAdmisionesPasos");
						break;
					case "documentos":
					case "documento":
					case "documentacion":
					case "documentación":
					case "papeles":
						session.beginDialog("guiaAdmisionesDocumentos");
						break;
					case "respuesta":
						session.beginDialog("guiaAdmisionesRespuesta");
						break;
					default:
						session.endDialog('aún no reconozco el tema de:'+session.userData.AdmInfo);
						session.userData.AdmInfo = null;
						session.beginDialog("guiaAdmisiones");
						break;
				}
			},
			/**
			 * pregunta si el usuario necesita más ayuda
			 */
			function (session, results) {
				session.userData.AdmInfo = null;
				builder.Prompts.confirm(session, "¿Quieres que te ayude con algo más del proceso de admisión?");
			},
			/**
			 * termina el diálogo o lo reinicia según la respuesta de la pregunta anterior
			 */
			function (session, results) {
				if (results.response) {
					session.endDialog("vale...");
					session.beginDialog("guiaAdmisiones");
				} else {
					session.userData.nivelGrado=null;
					session.endDialog("Vale, si necesitas algo sobre el proceso de admisión ya sabes que te puedo ayudar.");
				}
			}
		]
	}
}