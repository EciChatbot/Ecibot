module.exports = {
	getDialogo:function(parametros) { 		var builder=parametros._builder; 		var ecibot=parametros._ecibot; 		var request=parametros._request;
		return [
			/**
			 * muestra los pasos y pregunta si quiere ayuda paso a paso
			 */
			function (session, args, next) {
				if (ecibot.esNivel(ecibot.PREGRADO, session.userData.nivelGrado)) {
					var info=ecibot.admision.getPasos(session.userData.nivelGrado);
					session.send(info["pasos"]);
					session.send(
						new builder.Message(session).addAttachment(
							new builder.HeroCard(session)
								.text(info["ingBiomedica"])
								.buttons([
									builder.CardAction.openUrl(session, info["ingBiomedicaUrl"] , 'Ver pasos de Ing. Biomédica')
							])
						)
					);
				}else{
					session.send(ecibot.admision.getPasos(session.userData.nivelGrado));
				}
				builder.Prompts.confirm(session, "¿quieres que te iniciemos el proceso admisión?");
			},
			/**
			 * Desde qué paso
			 */
			function (session, results, next) {
				if (results.response) {
					builder.Prompts.choice(session, "¿Desde qué paso deseas iniciar?:", ecibot.admision.getPasosGuiado(session.userData.nivelGrado, "resumen").join("|"), { listStyle: 4 });
				} else {
					session.endDialog("Listo dejémolo para después.");
				}
			},
			/**
			 * según la respuesta anterior, termina el diálogo o inicia el paso a paso
			 */
			function (session, results, next) {
				session.dialogData.pasoDesde=results.response.index;
				console.log(session.dialogData.pasoDesde);
				if(session.dialogData.pasoDesde>0){
					session.dialogData.pasoDesde--;
					next();
				}else{
					if(session.dialogData.pasoDesde==0)session.dialogData.pasoDesde=-1;
					session.send(ecibot.admision.getPasosGuiado(session.userData.nivelGrado, 0));
					builder.Prompts.confirm(session, "si ya realizaste el paso anterior, ¿podemos continuar?");
				}
			},
			/**
			 * paso 1
			 */
			function (session, results, next) {
				console.log(session.dialogData.pasoDesde);
				if(session.dialogData.pasoDesde>0){
					session.dialogData.pasoDesde--;
					next();
				}else if (results.response || session.dialogData.pasoDesde==0) {
					if(session.dialogData.pasoDesde==0)session.dialogData.pasoDesde=-1;
					session.send(ecibot.admision.getPasosGuiado(session.userData.nivelGrado, 1));
					builder.Prompts.confirm(session, "si ya realizaste el paso anterior, ¿podemos continuar?");
				} else {
					session.endDialog("Listo dejémolo para después.");
				}
			},
			/**
			 * paso 2
			 */
			function (session, results, next) {
				console.log(session.dialogData.pasoDesde);
				if(session.dialogData.pasoDesde>0){
					session.dialogData.pasoDesde--;
					next();
				}else if (results.response || session.dialogData.pasoDesde==0) {
					if(session.dialogData.pasoDesde==0)session.dialogData.pasoDesde=-1;
					session.send(ecibot.admision.getPasosGuiado(session.userData.nivelGrado, 2));
					builder.Prompts.confirm(session, "si ya realizaste el paso anterior, ¿podemos continuar?");
				} else {
					session.endDialog("Listo dejémolo para después.");
				}
			},
			/**
			 * paso 3
			 */
			function (session, results, next) {
				console.log(session.dialogData.pasoDesde);
				if(session.dialogData.pasoDesde>0){
					session.dialogData.pasoDesde--;
					next();
				}else if (results.response || session.dialogData.pasoDesde==0) {
					if(session.dialogData.pasoDesde==0)session.dialogData.pasoDesde=-1;
					session.send(ecibot.admision.getPasosGuiado(session.userData.nivelGrado, 3));
					builder.Prompts.confirm(session, "si ya realizaste el paso anterior, ¿podemos continuar?");
				} else {
					session.endDialog("Listo dejémolo para después.");
				}
			},
			/**
			 * paso 4, luego preguntamos si quiere enviar los documentos
			 */
			function (session, results, next) {
				console.log(session.dialogData.pasoDesde);
				if(session.dialogData.pasoDesde>0){
					session.dialogData.pasoDesde--;
					next();
				}else if (results.response || session.dialogData.pasoDesde==0) {
					if(session.dialogData.pasoDesde==0)session.dialogData.pasoDesde=-1;
					session.send(ecibot.admision.getPasosGuiado(session.userData.nivelGrado, 4));
					builder.Prompts.confirm(session, "si ya realizaste el paso anterior, ¿Quieres enviar los documentos en línea?");
				} else {
					session.endDialog("Listo dejémolo para después.");
				}
			},
			/**
			 * preguntamos si tiene los documentos listos
			 */
			function (session, results, next) {
				console.log(session.dialogData.pasoDesde);
				if(session.dialogData.pasoDesde>0){
					session.dialogData.pasoDesde--;
					next();
				}else if (results.response || session.dialogData.pasoDesde==0) {
					if(session.dialogData.pasoDesde==0)session.dialogData.pasoDesde=-1;
					builder.Prompts.confirm(session, "¿tienes todos los documentos listos?");
				} else {
					session.endDialog("Alístalos y regresa cuando los tengas listos.");
				}
			},
			/**
			 * paso 5, envío de documentos
			 */
			function (session, results, next) {
				console.log(session.dialogData.pasoDesde);
				if(session.dialogData.pasoDesde>0){
					session.dialogData.pasoDesde--;
					next();
				}else if (results.response || session.dialogData.pasoDesde==0) {
					if(session.dialogData.pasoDesde==0)session.dialogData.pasoDesde=-1;
					session.send(ecibot.admision.getPasosGuiado(session.userData.nivelGrado, 5));
					builder.Prompts.confirm(session, "si ya realizaste el paso anterior, ¿podemos continuar?");
				} else {
					session.endDialog("Listo ten todos los documentos listos y vuelves cuando los tengas.");
				}
			},
			/**
			 * final
			 */
			function (session, results, next) {
				if (results.response) {
					session.endDialog("Eso es todo ya estás en proceso de admisión.");
				} else {
					session.endDialog("Listo dejémolo para después.");
				}
			},
		]
	}
}