var builder = require('botbuilder');
var dotenv = require('dotenv');
var ecibot = require('../extra/ecibot');
var request = require('ajax-request');

module.exports = [
	/**
	 * busca entidades y captura el tipo de admision que necesita el usuario, en caso que no las encuentre muestra opciones
	 */
	function (session, args, next) {
		// Buscar entidades
		if (args) {
			//var entidadAdm=builder.EntityRecognizer.findEntity(args.entities, 'Admision_tipo');
			var entidadAdmInfo = builder.EntityRecognizer.findEntity(args.entities, 'Admision_info');
			var nivelGrado = builder.EntityRecognizer.findEntity(args.entities, 'nivelGrado');
			session.userData.AdmInfo = entidadAdmInfo ? entidadAdmInfo.entity : undefined;
			session.userData.nivelGrado = nivelGrado ? nivelGrado.entity : undefined;
		}
		session.beginDialog("guiaAdmisiones");
	}
]