# **Manual Administrador**
---
## **QnA** *(Mantenimiendo y Administrador)*
- Para subir nuevas preguntas y respuestas a la base de conocimiento de QnA Maker se debe ingresar a: https://www.qnamaker.ai/old/

- Iniciar sesión con usuario y clave correspondientes y luego seleccionar la opción "My Services"

	![QnA1](Map/QnA1.JPG)

- Dentro de "My Services", modificar por medio del icono de lapiz el servicio "ECIBOT_FAQ"

	![QnA2](Map/QnA2.JPG)

- Cuando accedemos a la interfaz del servicio, tenemos la opción de visualizar la base de conocimiento, hacer pruebas y configuraciones, como vamos a subir nuevas preguntas y respuestas se selecciona la opción de "Settings"

	![QnA3](Map/QnA3.JPG)

- Las preguntas y respuestas estan alojadas en un archivo Excel con nombre "QnA FAQS Knowledge Base.xlsx" que se puede encontrar en el repositorio de Git del proyecto en la carpeta QnA. Las preguntas se agregan en la columna de "Preguntas" y en frente su respectiva respuesta.

	Guardamos el archivo y dentro de la plataforma de QnA, seleccionamos la opción "Select File..." y buscamos el archivo Excel.

	![QnA4](Map/QnA4.JPG)

	![QnA5](Map/QnA5.JPG)

- Para que tenga efecto el cambio y la base de conocimiento se actualice y se entrene, se debe seleccionar la opción de "Save and retrain". Para que las nuevas preguntas esten en ambiente de producción y accesibles procedemos después de entrenar a publicarlo esto por medio de la opción "Publish".

	![QnA6](Map/QnA6.JPG)

- En la pantalla ed confirmación nos mostrará la cantidad de preguntas nuevas en el archivo que se subio y posteriormente se debe seleccionar la opción "Publish" para confirmar.

	![QnA7](Map/QnA7.JPG)

- QnA nos debe confirmar que el servicio ha sido desplegado correctamente y nos recuerda el ID del servicio y las respectivas llaves para accederlo desde el bot. (Esto ya se encuentra configurado dentro de la logica de negocio del bot).

	![QnA8](Map/QnA8.JPG)

---
## **LUIS** *(Mantenimiendo y Administrador)*

-	Para modificar la inteligencia de **LUIS** ingresar a: https://www.luis.ai/home

-	Iniciar sesión y luego seleccionar el chatbot

	![Boceto](/uploads/49381537249a0c3fa5ea6260e7d9f4dd/Boceto.png)
-	A continuación se tendrán los intentos y entidades
	![](Map/luis1.PNG)
	### **Intentos**
	-	Se pueden agregar o modificar las reglas de entendimiento de los intentos usando la entrada de texto o el menú de opciones que se encuentra frente a cada entrada ya registrada
		![](Map/luis2.PNG)

	### **Entidades**
	-	Se pueden agregar o modificar el conjunto de entidades, actualmente se manejan 2 List y Simple, dónde la List ha sido alimentada con valores predefinidos y la Simple son valores abiertos que se van a ir alimentando de las reglas definidas en los intentos
		![](Map/luis3.PNG)
	-	Para el caso de las entidades simples se va a permitir en los intentos modificar haciendo clic sobre las palabras que pueden aplicar a dicho entidad como se muestra en la imagen
		![](Map/luis7.PNG)
	-	Luego de seleccionar las palabras que corresponden a la entidad se seleccionará la entidad TIPO SIMPLE que se haya definido anteriormente
		![](Map/luis8.PNG)
	-	De esta manera aprenderá a que en un futuro cuando se presente una entidad similar tomarla como un **TelefonoDe**
		![](Map/luis9.PNG)

	### **Revisión de entradas**
	-	En el Review de EndPonits, encontraremos las entradas que se han generado desde el uso del chatbot, dónde no existen dentro de los intentos, por ende podemos aprovar, desaprovar y ajustar las entradas hacia el intento correspondiente, de esta manera enseñarle a LUIS
		![](Map/luis4.PNG)
	-	En la columna de Intentos Asignados, seleccionamos en este caso noticias, cómo se observa en la imagen para poder orientarlo hacia el lugar correcto
		![](Map/luis5.PNG)
	-	Según sea el caso se puede usar la caneca o el chulo indicando la aceptación o el rechazo de la frase ingresada
		![](Map/luis6.PNG)

	### **Publicar mejora**
	-	Luego de realizar cambios, arriba a la derecha el  **Train** estará con un punto **rojo**, lo que indicará que aún no ha sido entrenado con los nuevos cambios.
		![](Map/luis10.PNG)
	-	Hacemos clic en él para luego publicar la versión, y cómo se observa en la siguiente imagen pasará a **color verde** de nuevo.
		![](Map/luis11.PNG)
	-	Finalmente le da clic en **PUBLISH** y luego en el botón azul **Publish**, cómo muestra la imagen.
		![](Map/luis12.PNG)
---
## **Administrador de reportes** *(Mantenimiendo y Administrador)*
### **Emergencias, Seguridad, Sugerencias y AudioVisuales**

* Para esta configuración se accede al archivo:

		servidor\bd\usuarios.json

* Que tendrá un contenido similiar a:

	```json
	{"emergencia":[{"id":1,"nombre":"Kvn Alvarado","usuario":"kevin@emergencia","clave":"81dc9bdb52d04dc20036dbd8313ed055"}],"audiovisuales":[{"id":1,"nombre":"Kvn Alvarado","usuario":"kevin@audiovisuales","clave":"81dc9bdb52d04dc20036dbd8313ed055"}],"seguridad":[{"id":1,"nombre":"Kvn Alvarado","usuario":"kevin@seguridad","clave":"81dc9bdb52d04dc20036dbd8313ed055"}],"sugerencias":[{"id":1,"nombre":"Kvn Alvarado","usuario":"kevin@sugerencias","clave":"81dc9bdb52d04dc20036dbd8313ed055"}]}
	```
* Para facilidades del manual vamos a mostrar la estructura sin comprimir:

	```json
	{
		"emergencia": [
			{
				"id": 1,
				"nombre": "Kvn Alvarado",
				"usuario": "kevin@emergencia",
				"clave": "81dc9bdb52d04dc20036dbd8313ed055"
			}
		],
		"audiovisuales": [
			{
				"id": 1,
				"nombre": "Kvn Alvarado",
				"usuario": "kevin@audiovisuales",
				"clave": "81dc9bdb52d04dc20036dbd8313ed055"
			}
		],
		"seguridad": [
			{
				"id": 1,
				"nombre": "Kvn Alvarado",
				"usuario": "kevin@seguridad",
				"clave": "81dc9bdb52d04dc20036dbd8313ed055"
			}
		],
		"sugerencias": [
			{
				"id": 1,
				"nombre": "Kvn Alvarado",
				"usuario": "kevin@sugerencias",
				"clave": "81dc9bdb52d04dc20036dbd8313ed055"
			}
		]
	}
	```
* Para facilidades de lectura y viendo la similitud del gestor de usuarios, se usará sólo el ejemplo de **emergencia**:

	```json
	{
		"emergencia": [
			{
				"id": 1,
				"nombre": "Kvn Alvarado",
				"usuario": "kevin@emergencia",
				"clave": "81dc9bdb52d04dc20036dbd8313ed055"
			}
		],
		...
	}
	```
	dónde se observa se tene un ID, Nombre, Usuario y Clave por cada persona, para el siguiente ejemplo se va a modificar agregando un usuario que pertenezca a emergencia nuevo con las siguientes características:

		id: 2
		nombre: Germán Pérez
		usuario: german@emergencia,
		clave: german124
	
	**IMPORTANTE #1:** se hace la aclaración que el usuario DEBE terminar en **@emergencia**, **@audiovisuales** o según sea el caso

	**IMPORTANTE #2:** la contraseña está cifrada con MD5 que para nuestro ejemplo usaremos el servicio: [clic acá](https://md5online.org/md5-encrypt.html), pero se puede usar cualquier otro que codifique MD5
		
	![](Map/md5.PNG)

	por lo tanto la clave cifrada quedaría:

		2c9b8bbdad59cfa289ac4494c197487c

* agregamos el usuario así:
	
	```json
	{
		"emergencia": [
			{
				"id": 1,
				"nombre": "Kvn Alvarado",
				"usuario": "kevin@emergencia",
				"clave": "81dc9bdb52d04dc20036dbd8313ed055"
			},
			{
				"id": 2,
				"nombre": "Germán Pérez",
				"usuario": "gernan@emergencia",
				"clave": "2c9b8bbdad59cfa289ac4494c197487c"
			}
		],
		...
	}
	```

* finalmente vusalizando con el archivo original quedaría algo cómo:
	```json
	{"emergencia": [{"id": 1,"nombre": "Kvn Alvarado","usuario": "kevin@emergencia","clave": "81dc9bdb52d04dc20036dbd8313ed055"},{"id": 2,"nombre": "Germán Pérez","usuario": "gernan@emergencia","clave": "2c9b8bbdad59cfa289ac4494c197487c"}],"audiovisuales": [{"id": 1,"nombre": "Kvn Alvarado","usuario": "kevin@audiovisuales","clave": "81dc9bdb52d04dc20036dbd8313ed055"}],"seguridad": [{"id": 1,"nombre": "Kvn Alvarado","usuario": "kevin@seguridad","clave": "81dc9bdb52d04dc20036dbd8313ed055"}],"sugerencias": [{"id": 1,"nombre": "Kvn Alvarado","usuario": "kevin@sugerencias","clave": "81dc9bdb52d04dc20036dbd8313ed055"}]}
	```

* Una vez guardado el archivo se pude probar el acceso ingresando las credenciales en la ruta:

		http://chatbot.is.escuelaing.edu.co/admin

	ingresando los datos con éxito así:

	![](Map/admin.PNG)