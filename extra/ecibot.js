/**
 * ecibot info "base de datos" local -> stub
 */
ecibot_=(function(params) {
	var _LUISAPI="";
	var _LUISKEY="";
	var _SERVER="chatbot.is.escuelaing.edu.co";//serveo.net:7999";
	var _TEL_AUDIOVISUALES="316683600";
	var _niveles=Array("pregrado","posgrado");
	var _PREGRADO=_niveles[0];
	var _POSGRADO=_niveles[1];
	var _carreras=Array("Ing. Biomedica","Ing. Ambiental","Ing. Sistemas");

	//Admision
	var _admision=Array("_pasosAdmision","_valorAdmision","_requisitosAdmision");
		//documentos admision
		_admision["_documentosAdmision"]=Array(_PREGRADO,_POSGRADO);
			_admision["_documentosAdmision"][_PREGRADO]="Los documentos requeridos son:\n- **Foto** 3x4.\n- **Documento aspirante y acudiente** ampliada al 150%.\n- **Carta de acudiente**. [Descárgala acá](http://www.escuelaing.edu.co/uploads/descargables/1156_carta_acudiente.doc)\n- Si eres Bachiller Extranjero debes entregar la **homologacion Saber 11** ante el ICFES, [clic aquí](http://www2.icfes.gov.co/atencion-al-ciudadano/examenes-homologables).\n- **Soporte de ingresos** familiares, revísalos en PASO 3 de [este enlace](http://www.escuelaing.edu.co/es/programas/admisiones/pregrado?id=1&itemId=3)";
			_admision["_documentosAdmision"][_POSGRADO]="Los documentos requeridos son:\n\n- **Foto** 3x4.\n- **Documento aspirante y acudiente** ampliada al 150%.\n- **Carta de presentación** del aspirante (máx 1 pág).\n- **Hoja de vida** resumida (máx 2 págs).\n- **Fotocopia tarjeta profesional** o certificado de documento en trámite.\n- **Fotocopia diploma** o acta de grado.\n- **Fotocopia del certificado de notas** del pregrado y promedio de carrera.\n- 2 **cartas de recomendación**.";
		//pasos admision
		_admision["_pasosAdmision"]=Array(_PREGRADO,_POSGRADO);
			_admision["_pasosAdmision"][_PREGRADO]=Array("simple","guiado");
			_admision["_pasosAdmision"][_POSGRADO]=Array("simple","guiado");
			_admision["_pasosAdmision"][_PREGRADO]["simple"]=Array("pasos","ingBiomedica","ingBiomedicaUrl");
			//pregrado
			_admision["_pasosAdmision"][_PREGRADO]["simple"]["pasos"]="Son 3 simples pasos:\n\n- Diligencia tus datos básicos para obtener el Pin.\n- Pagar tu inscripción ($99.000) con tu Pin creado, para diligenciar tu formulario.\n- Enviar documentos.";
			_admision["_pasosAdmision"][_PREGRADO]["simple"]["ingBiomedica"]="Para "+_carreras[0]+" el proceso es distinto";
			_admision["_pasosAdmision"][_PREGRADO]["simple"]["ingBiomedicaUrl"]="http://admisiones.escuelaing.edu.co/Biomedica/";
			//posgrado
			_admision["_pasosAdmision"][_POSGRADO]["simple"]="Son 3 simples pasos:\n\n1. Diligencia tus datos básicos para obtener el Pin.\n2. ingresar para diligenciar tu formulario.\n3. Enviar documentos.";

			//paso a paso guiado PREGRADO
			_admision["_pasosAdmision"][_PREGRADO]["guiado"]=Array();
				_admision["_pasosAdmision"][_PREGRADO]["guiado"]["resumen"]=Array("Solicitar número de referencia","Consultar número de referencia","Pago de inscripción","Diligenciar formulario","Documentación","Enviar documentos");
				_admision["_pasosAdmision"][_PREGRADO]["guiado"][0]="Solicita tu número de referencia [acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/ValidaExtranjero?tipoest=N) \n\n y Realiza la activación de la Admisión [acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/ProcNoMatric)";
				_admision["_pasosAdmision"][_PREGRADO]["guiado"][1]="Consulta número de referencia [acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/Consulta)";
				_admision["_pasosAdmision"][_PREGRADO]["guiado"][2]="Realiza el pago de la  incripción [aquí](http://tycho.escuelaing.edu.co/PagosenLinea/loginAdmitido?id=1), si deseas más información de las [formas de pago](http://www.escuelaing.edu.co/es/programas/admisiones/pregrado?id=1&itemId=3)";
				_admision["_pasosAdmision"][_PREGRADO]["guiado"][3]="En el momento de diligenciar el formulario debes conocer:\n\n- Valor de la pensión del colegio.\n\n- Ingresos, actividad y ocupación de tus padres.\n\nNota: **Diligencia el formulario despacio y con cuidado, porque no podrás hacer modificaciones**.\n\npara diligenciar [clic acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/SolicitudAdm?tipoest=N)";
				_admision["_pasosAdmision"][_PREGRADO]["guiado"][4]=_admision["_documentosAdmision"][_PREGRADO];
				_admision["_pasosAdmision"][_PREGRADO]["guiado"][5]="Verifica el estado de tus documentos [acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/ConsultaDoc)\n\nPara finalizar envía los documentos por cualquiera de los siguientes medios:\n- Personalmente\n- En línea\n- Correo Certificado\n- Correo electrónico\n\nRevisa el final del paso 3 en: [Enviar documentos](http://www.escuelaing.edu.co/es/programas/admisiones/pregrado?id=1&itemId=3)";

			//paso a paso guiado POSGRADO
			_admision["_pasosAdmision"][_POSGRADO]["guiado"]=Array();
				_admision["_pasosAdmision"][_POSGRADO]["guiado"]["resumen"]=Array("Solicitar número de referencia","Consultar número de referencia","Diligenciar formulario","Documentos","Carta de recomendación","Enviar documentos");
				_admision["_pasosAdmision"][_POSGRADO]["guiado"][0]="Solicita tu número de referencia [acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/Posgrados)";
				_admision["_pasosAdmision"][_POSGRADO]["guiado"][1]="Consulta Número de Referencia [acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/Consulta)"; 
				_admision["_pasosAdmision"][_POSGRADO]["guiado"][2]="Al momento de diligenciar el formulario debes conocer:\n\n- Información Laboral.\n- Historia Académica.\n\nIniciemos con el formulario haciendo [clic acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/InicioPosgrado)"; 
				_admision["_pasosAdmision"][_POSGRADO]["guiado"][3]=_admision["_documentosAdmision"][_POSGRADO];
				_admision["_pasosAdmision"][_POSGRADO]["guiado"][4]="Cuando seas admitido, deberás presentar el documento original del certificado de calificaciones de pregrado para legalizar la matricula (sólo si eres graduado de una institución distinta a la escuela).\n\n- 2 recomendaciones cargadas [acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/ReferenciasPos), enviado a las personas que lo recomiendan.\n\nSi lo deseas hacer en forma física debes adjuntar las recomendaciones usando [este formato](http://www.escuelaing.edu.co/uploads/descargables/1667_cartadereferenciaposgrados.pdf)"; 
				_admision["_pasosAdmision"][_POSGRADO]["guiado"][5]="Para enviar en línea crear archivos con extensión .pdf y adjuntarlos [acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/ValidaPos)";
		//requisitos
		_admision["_requisitosAdmision"]=Array();
			_admision["_requisitosAdmision"][_PREGRADO]=Array();
			_admision["_requisitosAdmision"][_POSGRADO]=Array();
			//pregrado
			_admision["_requisitosAdmision"][_PREGRADO]["minimo"]="Debes tener minimo 45 puntos en el examen Saber 11, según el programa académico en:\n\n\n**INGENIERIAS Y MATEMÁTICAS:** Ciencias Naturales, Lectura Crítica y Matemáticas\n\n\n**ADMINISTRACIÓN DE EMPRESAS  Y ECONOMÍA:** Sociales y Ciudadanas, Lectura Crítica y Matemáticas";
			_admision["_requisitosAdmision"][_PREGRADO]["url"]="http://www.escuelaing.edu.co/es/programas/admisiones/pregrado?id=1&itemId=2";
			_admision["_requisitosAdmision"][_PREGRADO]["extranjero"]="Lee según sea tu caso:\n\n**1. Título de bachiller colombiano** con nacionalidad extranjera, debes entregar el documento de identidad expedido por Migración Colombia.\n\n**2. Título de bachiller de otro país**, debes entregar el exámen Saber 11 que aplique a cada pais homologado ante el ICFES.\n\nTramitar la convalidación del título de bachiller que se entrega en el momento de la firma de matrícula, para realizarlo requiere apostillar el título y seguir el proceso determinado para convalidación ante el Ministerio de Educación en Colombia.";
			_admision["_requisitosAdmision"][_PREGRADO]["url2"]="http://www.mineducacion.gov.co/1759/w3-article-350670.html";
			//posgrado
			_admision["_requisitosAdmision"][_POSGRADO]["minimo"]="Debes ser profesional de una institución de educación superior.";
			_admision["_requisitosAdmision"][_POSGRADO]["extranjero"]="Debes entregar el documento de indentidad vigente expedido por Migración Colombia y el documento que lo certifique convalidado.";
			_admision["_requisitosAdmision"][_POSGRADO]["url2"]="http://www.mineducacion.gov.co/1759/w3-article-350670.html";
		//respuesta
		_admision["_respuestaAdmision"]=Array();
			_admision["_respuestaAdmision"][_PREGRADO]=Array();
			_admision["_respuestaAdmision"][_POSGRADO]=Array();
			//pregrado
			_admision["_respuestaAdmision"][_PREGRADO][false]="Para consultar el proceso de admisión [clic acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/Admitidos?prog=AN)";
			//posgrado
			_admision["_respuestaAdmision"][_POSGRADO][false]="Para consultar el proceso de admisión [clic acá](http://admisiones.escuelaing.edu.co/inscripcioneslinea/ConsultaRespuesta)";
			_admision["_respuestaAdmision"][_POSGRADO][true]="Si eres estudiante extranjero, tendrás un tiempo máximo que define el Comité para formalizar tu ingreso se realiza presentando los siguientes documentos:\n\n- Visa estudiantil, la cual debe estar autorizada para realizar estudios en la Escuela.\n\nPara conocer el trámite de solicitud de la visa ingresa al siguiente [vínculo](www.mineducacion.gov.co/1759/w3-article-350670.html)";

	/**
	 * damos acceso a module.exports de usar los siguientes datos
	 */
	return {
		LUISAPI:_LUISAPI,
		LUISKEY:_LUISKEY,
		niveles:_niveles,
		SERVER:_SERVER,
		TEL_AUDIOVISUALES:_TEL_AUDIOVISUALES,
		PREGRADO:_PREGRADO,
		POSGRADO:_POSGRADO,
		carreras:_carreras,
		requisitosAdmision:_admision["_requisitosAdmision"],
		documentos:_admision["_documentosAdmision"],
		pasosAdmision:_admision["_pasosAdmision"],
		respuestaAdmision:_admision["_respuestaAdmision"],
	}
})();

/**
 * funciones públicas que son accesibles desde app.js
 */
module.exports={
	SERVER:ecibot_.SERVER,
	TEL_AUDIOVISUALES:ecibot_.TEL_AUDIOVISUALES,
	PREGRADO:ecibot_.PREGRADO,
	POSGRADO:ecibot_.POSGRADO,
	CARRERAS:{
		BIOMEDICA:ecibot_.carreras[0],
		AMBIENTAL:ecibot_.carreras[1],
		SISTEMAS:ecibot_.carreras[2],
	},
	/**
	 * establece info de luis para hacer consultas extra
	 */
	setLUIS:function(api,key) {
		ecibot_.LUISAPI=api;
		ecibot_.LUISKEY=key;
	},
	/**
	 * return key de luis para hacer consultas extra
	 */
	getLUISKEY:function() {
		return ecibot_.LUISKEY;
	},
	/**
	 * return api de luis para hacer consultas extra
	 */
	getLUISAPI:function() {
		return ecibot_.LUISAPI;
	},
	/**
	 * indica si el nivel que pide es el que se tiene de información hasta el momento
	 */
	esNivel:function(nivelPide,nivelTiene) {
		return nivelPide.toLowerCase()==nivelTiene.toLowerCase();
	},
	/**
	 * retorna el arreglo de opciones de niveles de grado
	 */
	opcionesNivelGrado:function() {
		return ecibot_.niveles;
	},
	admision:{
		getRequisitos:function(nivelPide,info) {
			return ecibot_.requisitosAdmision[nivelPide][info];
		},
		getDocumentos:function(nivelPide) {
			return ecibot_.documentos[nivelPide];
		},
		getPasos:function(nivelPide,conGuia,carrera) {
			var masInfo=!conGuia?"simple":"guiado";
			return ecibot_.pasosAdmision[nivelPide][masInfo];
		},
		getPasosGuiado:function(nivelPide,nPaso){
			return ecibot_.pasosAdmision[nivelPide]["guiado"][nPaso];
		},
		getRespuesta:function(nivelPide,extranjero){
			return ecibot_.respuestaAdmision[nivelPide][extranjero];
		},
	}
};