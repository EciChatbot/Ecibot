/** 
 * administrador
*/
var admin = (function () {
	var x = {
		url: "Admin",
		funcion: "admin"
	};
	var urlReportes="/reportes";

	return {
		/**
		 * función que se ejecuta al iniciar el sitio
		 */
		init:function () {
			setTimeout(admin.GET,200);
		},
		/**
		 * revisa si el usuario ya inició sesión
		 */
		GET: function (params) {
			MJ_load(true);
			//creamos otra variable para NO usar la global
			var z = {
				url: x.url,
				funcion: x.funcion
			};
			z.type = "GET";
			z.data = "";
			z.selfCallback = admin.GET;
			z.selfCallbackVars = params;
			z.json = true;
			z.callback = function (J) {
				if (J["error"]) {//si hay error
					MJ_simple("Accediendo al administrador", J["mj"]);
				}else if (!J["res"]) {// si no inició
					setTimeout(function(){MJ_load(false);},1000);
					return false;
				}else location.href=urlReportes;//si ya inició
			};
			Ajax_(z);
		},
		/**
		 * encargado de permitir iniciar sesión
		 */
		POST: function (params) {
			var titulo = "Ingresar al administrador";
			var Usuario = $("#Usuario").val();
			var Clave = $("#Clave").val();

			if (Usuario == "" || Clave == "") {
				MJ_simple(titulo, "Completa el usuario y la clave");
				return false;
			}

			MJ_load(true);
			//creamos otra variable para NO usar la global
			var z = {
				url: x.url,
				funcion: x.funcion
			};
			z.type = "POST";
			z.data = "Usuario=" + Usuario + "&Clave=" + Clave;
			z.selfCallback = admin.POST;
			z.selfCallbackVars = params;
			z.json = true;
			z.callback = function (J) {
				if (!J["res"]) {
					MJ_simple(titulo,J["mj"]);
					return false;
				}else location.href=urlReportes;
			};
			Ajax_(z);
		}
	}
})();
admin.init();