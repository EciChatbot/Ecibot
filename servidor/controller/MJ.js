/*
--------------------------------------------
Versión 2.0 encargado de mostrar los mensajes en la pantalla todos empiezan por MJ_...
--------------------------------------------
*/
var tmpDomain = document.domain;
var MJcolorBorde = "333333";
var MJcolorFondo = "EEEEEE";
var MJcolorSup = "444444";
var MJclassError = "input_error";
var MJclassCorrecto = "input_normal";
var MJcolorClaro = "DDDDDD";
var MJcolorOscuro = "333333";

function MJ_input(q, id) {
	/*
	function MJ_input(bool q,str id) 
	se encarga de cambiar el color de los campos de pendiendo del estado que tenga q
	q: estado de error (false) o correcto (true) que tenga el campo id
	id: id del campo
	*/
	document.getElementById(id).className = ((!q) ? MJclassError : MJclassCorrecto);
}

function MJ_iniciar(q) {
	if (!document.getElementById("CSSMJ")) {
		var tmp = document.createElement('link');
		tmp.id = 'CSSMJ';
		tmp.type = 'text/css';
		tmp.rel = 'stylesheet';
		tmp.href = 'S/MJ.php?claro=' + MJcolorClaro + '&oscuro=' + MJcolorOscuro;
		document.head.appendChild(tmp);
	}
	if (!document.getElementById("div_aviso")) {
		$('<div id="div_aviso" style="display:none;"></div><div id="div_aviso2" style="display:none;"></div></div>').appendTo("body");
	} else {
		document.getElementById('div_aviso').style.cursor = "";
		document.getElementById('div_aviso2').style.cursor = "";
	}
	$("body").css("overflow", "hidden");
	$("#div_aviso").fadeIn(500, function () {
		$("#div_aviso2").fadeIn(500);
	});
}
function MJ_terminar() {
	$("#div_aviso2").fadeOut(500, function () {
		$("#div_aviso").fadeOut(500, function () {
			$("body").css("overflow", "auto");
		});
		document.getElementById('div_aviso2').innerHTML = '';
	});
}
function MJ_simplex(titulo, mensaje, sinBotonX) {
	MJ_iniciar();
	var style = "";
	var t = '<input type="button" value="X" class="MJ_input_color" onclick="MJ_terminar();">';
	if (sinBotonX) t = '&nbsp;';
	document.getElementById('div_aviso2').innerHTML = '<table bgcolor="#' + MJcolorFondo + '" rules="none" bordercolor="#' + MJcolorBorde + '" style="' + style + '" border="2" align="center" cellpadding="0" cellspacing="0">  <tr bgcolor="#' + MJcolorSup + '"><td width="30">' + t + '</td><td align="center" class="MJ_text_mayor_white">' + titulo + '</td>  </tr>  <tr>    <td  colspan="2" align="center" class="MJ_text_normal"><br>' + mensaje + '</td>  </tr></table>';
}
function MJ_simple(titulo, mensaje) {
	MJ_iniciar();
	var style = "";
	document.getElementById('div_aviso2').innerHTML = '<table bgcolor="#' + MJcolorFondo + '" rules="none" bordercolor="#' + MJcolorBorde + '" style="' + style + '" border="2" align="center" cellpadding="0" cellspacing="0">  <tr>    <td align="center" bgcolor="#' + MJcolorSup + '" class="MJ_text_mayor_white">' + titulo + '</td>  </tr>  <tr>    <td align="center" class="MJ_text_normal"><br>' + mensaje + '<br><br><br></td>  </tr>  <tr>    <td align="left" class="MJ_text_mini_red">presiona ENTER para continuar.</td>  </tr>  <tr>    <td align="center"><input type="button" onClick="MJ_terminar();" class="MJ_input_gris" id="aceptar_aviso"  value="Aceptar"/></td>  </tr></table>';
	if ($("#dispositivo").val() != "m") document.getElementById('aceptar_aviso').focus();
}
function MJ_sino(titulo, mensaje, funcionSI, funcionNO) {
	MJ_iniciar();
	var style = "";
	if (funcionNO == "") { funcionNO = "MJ_terminar()" }
	document.getElementById('div_aviso2').innerHTML = '<table bgcolor="#' + MJcolorFondo + '" rules="none" bordercolor="#' + MJcolorBorde + '" style="' + style + '" border="2" align="center" cellpadding="0" cellspacing="0">  <tr>    <td align="center" bgcolor="#' + MJcolorSup + '" class="MJ_text_mayor_white">' + titulo + '</td>  </tr>  <tr>    <td align="center" class="MJ_text_normal"><br>' + mensaje + '<br><br><br></td>  </tr>  <tr>    <td align="left" class="MJ_text_mini_red">presiona ENTER para SI o TAB luego ENTER para NO</td>  </tr>  <tr>    <td align="center"><input type="button" onClick="' + funcionSI + 'MJ_terminar();" class="MJ_input_gris" id="aceptar_aviso"  value="SI"/> <input type="button" onClick="' + funcionNO + '" class="MJ_input_gris" value="NO"/></td>  </tr></table>';
	if ($("#dispositivo").val() != "m") document.getElementById('aceptar_aviso').focus();
}
function MJ_aceptar_cancelar(titulo, mensaje, funcionA, funcionC) {
	MJ_iniciar();

	var style = "";
	var funcionNO = funcionC;
	if (funcionC == "") { funcionNO = "MJ_terminar()" }
	document.getElementById('div_aviso2').innerHTML = '<table bgcolor="#' + MJcolorFondo + '" rules="none" bordercolor="#' + MJcolorBorde + '" style="' + style + '" border="2" align="center" cellpadding="0" cellspacing="0">  <tr>    <td align="center" bgcolor="#' + MJcolorSup + '" class="MJ_text_mayor_white">' + titulo + '</td>  </tr>  <tr>    <td align="center" class="MJ_text_normal"><br>' + mensaje + '<br><br><br></td>  </tr>  <tr>    <td align="left" class="MJ_text_mini_red">presiona ENTER para Aceptar o TAB luego ENTER para Cancelar </td>  </tr>  <tr>    <td align="center"><input type="button" onClick="' + funcionA + '" class="MJ_input_gris" id="aceptar_aviso"  value="Aceptar"/> <input type="button" onClick="' + funcionNO + '" class="MJ_input_gris" value="Cancelar"/></td>  </tr></table>';
	if ($("#dispositivo").val() != "m") document.getElementById('aceptar_aviso').focus();
}
function MJ_load(func) {
	var style = "";
	if (func) {
		MJ_iniciar(1);
		document.getElementById('div_aviso').style.cursor = "wait";
		document.getElementById('div_aviso2').style.cursor = "wait";
		document.getElementById('div_aviso2').innerHTML = '<table bgcolor="#' + MJcolorFondo + '" rules="none" bordercolor="#' + MJcolorBorde + '" style="' + style + '" border="2" align="center" cellpadding="0" cellspacing="0">  <tr>    <td align="center" bgcolor="#' + MJcolorSup + '" class="MJ_text_mayor_white">Espera por favor</td>  </tr>  <tr>    <td align="center" class="MJ_text_normal">Cargando ...</td>  </tr>  <tr>    <td align="left" class="MJ_text_mini_red">Espera por favor.</td>  </tr></table>';
	} else {
		MJ_terminar();
		document.getElementById('div_aviso').style.cursor = "";
		document.getElementById('div_aviso2').style.cursor = "";
	}
}
function MJ_frame(titulo, src, w, h) {
	var style = "";
	MJ_iniciar();
	document.getElementById('div_aviso2').innerHTML = '<table  bgcolor="#' + MJcolorFondo + '" rules="none" bordercolor="#' + MJcolorBorde + '"  style="' + style + '" border="1" cellpadding="0" cellspacing="0"><tr><td width="10"><input align="left" type="button" class="MJ_input_gris" value="X" title="Cerrar" onclick="MJ_terminar();" /></td><td align="center" bgcolor="#' + MJcolorSup + '" class="MJ_text_mayor_white">' + titulo + '</td></tr><tr><td colspan="2" bgcolor="#000000" ><iframe src="' + src + '" width="' + w + '" height="' + h + '" frameborder="0" framespacing="0" scrolling="auto" border="0" ></iframe></td></tr></table>';
}

/*---------------------------------*/
function MJ_propio(titulo, contenido) {

	var style = "";
	MJ_iniciar();
	document.getElementById('div_aviso2').innerHTML = '<table rules="none" bordercolor="#' + MJcolorBorde + '" style="' + style + '" border="2" align="center" cellpadding="0" cellspacing="0">  <tr>  <td width="20"><input align="left" type="button" class="MJ_input_gris" value="X" title="Cerrar" onclick="MJ_terminar();" /></td>  <td align="center" bgcolor="#' + MJcolorSup + '" class="MJ_text_mayor_white">' + titulo + '</td>  </tr>  <tr>    <td colspan="2" align="center" bgcolor="#000000">' + contenido + '</td>  </tr></table>';
}

function MJ_reTry(f) {
	if (document.getElementById("div_aviso") && document.getElementById("div_aviso2") && document.getElementById("div_aviso").style.display == 'block' && document.getElementById("div_aviso2").style.display == 'block')
		MJ_sino("Error de conexi&oacute;n", "Parece que no tiene conexi&oacute;n a internet<br><br>&iquest;Deseas intentar nuevamente?", "setTimeout(function(){" + f + ";},1000);", "MJ_terminar();");
	else if (confirm("Parece que no tienes conexion a internet. Deseas intentar nuevamente?")) eval(f + ";");
}