<?
/**
clase encargada de gestionar archivos con contenido JSON
 */
class ArchivoJSON{

	private $archivo;

	function __construct($archivo) {
		$this->archivo=$archivo;
	}

	/**
	Escribe contenido dentro del archivo en JSON recibe un Array como contenido
	*/
	public function escribir($contenido){
		$f=fopen($this->archivo, "w+");
		fwrite($f, json_encode($contenido));
		fclose($f);
	}

	/**
	abre el archivo y retorna el contenido en Array
	*/
	public function leer(){
		if(!file_exists($this->archivo)){
			$f=fopen($this->archivo, "w+") or die("No se pudo crear el archivo");
			$bd=array();
			fwrite($f, json_encode($bd));
			fclose($f);
		}
		//como ya estamos seguros que existe lo abrimos y obtenemos su contenido
		$f=fopen($this->archivo, "r");
		$bd=json_decode(fread($f,filesize($this->archivo)+1),true);
		fclose($f);
		return $bd;
	}
}
?>