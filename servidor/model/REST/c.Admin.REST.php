<?
require_once "../c.REST.php";
require_once "../c.ArchivoJSON.php";

class Admin extends REST{

	private $archivo="../../bd/usuarios.json";

	/**
	revisa si ya inició sesión
	 */
	function get(){
		$this->json("entidad",$_COOKIE["entidad"]);
		$this->res(isset($_COOKIE["idUsuario"]) && isset($_COOKIE["entidad"]));
		$this->send();
	}

	/**
	cierra sesión
	 */
	function delete(){
		setcookie("idUsuario", false, time()-1, "/");
		setcookie("entidad", false, time()-1, "/");
		$this->res(true);
		$this->send();
	}

	/**
	permite iniciar sesión
	 */
	function post(){
		//estructura del usuario debe ser: 
		//kevin@emergencia
		$Usuario=$this->DATA["Usuario"];
		$Clave=md5($this->DATA["Clave"]);
		$entidad=explode("@",$Usuario)[1];

		//definimos el valor de respuesta de json false
		$this->res(false);
		if($Usuario=="" || $Clave=="")$this->mj("Diligencia bien tus datos.");
		else{
			//estructura del archivo: {entidad:[usuario1,usuario..],entidad2:...}
			$ajson=new ArchivoJSON($this->archivo);
			$bd=$ajson->leer();
			if(isset($bd[$entidad])){
				//recorremos los usuarios de la entidad
				$usuarioEncontrado=false;
				foreach ($bd[$entidad] as $user)
					if($user["usuario"]==$Usuario){//si existe el usuario
						$usuarioEncontrado=true;
						if($user["clave"]==$Clave){//si la clave está ok
							$this->res(true);
							setcookie("idUsuario", $user["id"], time()+3600*24, "/");
							setcookie("entidad", $entidad, time()+3600*24, "/");
						}else $this->mj("Clave incorrecta.");
					}
				if(!$usuarioEncontrado)$this->mj("Usuario incorrecto.");
			}else $this->mj("Usuario incorrecto.");//enrealidad es la entidad pero no se le dice al usuario
		}
		$this->send();
	}
}
new Admin(true);
?>