// ############################### Importación de archivos necesarios #####################################
var dotenv = require('dotenv');
var restify = require('restify');
var builder = require('botbuilder');
var cognitiveservices = require('botbuilder-cognitiveservices');
var botbuilder_azure = require("botbuilder-azure");
var request = require('ajax-request');
var ecibot = require('./extra/ecibot');
var ecibotAjaxJson = require('./extra/ecibotAjaxJson');


// ############################### Inicio Configuracion #####################################
// Configuracion de la conexión con servidor Restify 
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
	console.log("%s Escuchando %s", server.name, server.url);
});

/*----------------------------------------------------------------------------------------
* Bot Storage: This is a great spot to register the private state storage for your bot. 
* We provide adapters for Azure Table, CosmosDb, SQL Azure, or you can implement your own!
* For samples and documentation, see: https://github.com/Microsoft/BotBuilder-Azure
* ---------------------------------------------------------------------------------------- */

var tableName = 'botdata';
var storageName = "ecibotstorage"; // Obtain from Azure Portal
var storageKey = "PjUgg02E9J6syYrCtg8AanVRWpEV2pTBOKRBwSYlV78di/SKq5tkXsn4aMlocy4AxIzXRdvLJzi4VExY2i6gGA=="; // Obtain from Azure Portal
var azureTableClient = new botbuilder_azure.AzureTableClient(tableName, storageName, storageKey);
var tableStorage = new botbuilder_azure.AzureBotStorage({ gzipData: false }, azureTableClient);

// Crear conector para el chat con el Framework (Credenciales de BotFramework para canales)
var connector = new builder.ChatConnector({
    appId: '24628101-ef0b-4c7b-9a0a-d420ffe5a972',
    //process.env.MICROSOFT_APP_ID,
    appPassword: 'vbtKHHQLT74;;cxxpR597!_'
    //process.env.MICROSOFT_APP_PASSWORD''
});

// Oimos mensajes del usuario
var bot = new builder.UniversalBot(connector, {
    localizerSettings: { 
        Locale: "es-ES",
        defaultLocale: "es-ES" 
    }
});

bot.set('storage', tableStorage);

server.post('/api/messages', connector.listen());

// Para utilizar variables de entorno
dotenv.config();

// QnA Maker
var qnarecognizer = new cognitiveservices.QnAMakerRecognizer({
    knowledgeBaseId: process.env.QNA_KNOWLEDGE_BASE_ID,
    subscriptionKey: process.env.QNA_SUBSCRIPTION_KEY,
	top: 2});
	
// API LUIS 
var luisApp = process.env.LUIS_APP;
var luisKey = process.env.LUIS_KEY;
var model = "https://eastus.api.cognitive.microsoft.com/luis/v2.0/apps/" + luisApp + "?subscription-key=" + luisKey + "&verbose=true&timezoneOffset=0&q=";
ecibot.setLUIS(luisApp,luisKey);
var recognizer = new builder.LuisRecognizer(model);

var dialog = new builder.IntentDialog({ recognizers: [recognizer, qnarecognizer] });
bot.dialog('/', dialog).endConversationAction(
    "endConversacion", "😊 Ok!, aquí estaré. Escribe 'Menu' y te mostraré los servicios en que puedo ayudarte",
    {
        matches: /^cancelar$|^chao$|^salir$|^adios$/i,
    }
);
// ############################### FIN Configuracion #####################################
bot.dialog('Saludar', require('./dialog/Saludar'));
bot.dialog('Comunicar', require('./dialog/Comunicar'));
bot.dialog('Admisiones', require('./dialog/Admisiones'));
bot.dialog('Emergencia', require('./dialog/Emergencia'));
bot.dialog('Sugerencia', require('./dialog/Sugerir'));
bot.dialog('Horario', require('./dialog/Horario'));
bot.dialog('Default', require('./dialog/Default'));
bot.dialog('Audiovisual', require('./dialog/Audiovisual'));
bot.dialog('CambioNombre', require('./dialog/CambioNombre'));
bot.dialog('Noticias', require('./dialog/NoticiasApi'));
bot.dialog('Clima', require('./dialog/ClimaApi'));
//=========================================================
// Este es el Default, cuando LUIS no entendió la consulta.
dialog.onDefault(
    function (session) { 
        session.beginDialog('Default')
    }
);
// Dialogos con LUIS
// Saludo y menu
dialog.matches('Saludar', [function (session, args) { 
    var score = args.score;
    if (score > 0.60){session.beginDialog('Saludar');} else{session.beginDialog('Default');}
}]);
//Directorio telefonico
dialog.matches('Comunicar', [function (session, args) { session.beginDialog('Comunicar', {intent: args.intent, entities: args.entities});}]);
// Admisiones (MODIFICAR)
dialog.matches('Admisiones', [function (session, args) { session.beginDialog('Admisiones', {intent: args.intent, entities: args.entities});}]);
// Soporte de emergencias
dialog.matches('Emergencia', [function (session, args) { session.beginDialog('Emergencia', {intent: args.intent, entities: args.entities});}]);
// Soporte para sugerencias
dialog.matches('Sugerir', [function (session, args) { session.beginDialog('Sugerencia', {intent: args.intent, entities: args.entities});}]);
// Soporte FAQS
dialog.matches('FAQ', [function (session, args) {
    var msg = new builder.Message(session)
    .text("Con mucho gusto, ¿Qué pregunta tienes?, te recuerdo que debe ser sobre cancelaciones, información sobre la universidad o proceso de matrículas. ❓")
    .suggestedActions(
        builder.SuggestedActions.create(
                session, [
                    builder.CardAction.imBack(session, "¿Cómo puedo llegar a la escuela?", "¿Cómo puedo llegar a la escuela?"),
                    builder.CardAction.imBack(session, "¿Cuántas materias puedo cancelar?", "¿Cuántas materias puedo cancelar?"),
                    builder.CardAction.imBack(session, "¿Qué carreras profesionales tienen?", "¿Qué carreras profesionales tienen?")
                ]
            ));
    session.endDialog(msg);
}]);
// Soporte Audiovisuales
dialog.matches('ServicioAudioVisual', [function (session, args) { session.beginDialog('Audiovisual', {intent: args.intent, entities: args.entities});}]);
// Soporte Consulta de Horarios
dialog.matches('Horario', [function (session, args) { session.beginDialog('Horario', {intent: args.intent, entities: args.entities});}]);
// OJO: Esto es un plus, Soporte Cronograma Calendario
dialog.matches('Calendario', [function (session, args) {
    session.send("Aquí lo tienes.. 😊");
    var msg = new builder.Message(session)
        .text("Calendario ECI 2018")
        .attachments([{
            contentType: "image/png",
            contentUrl: "http://sostenibilidadyestrategia.com/wp-content/uploads/2018/02/calendario.png"
        }]);
    session.endConversation(msg);
    }]
);
// Match donde el bot se siente irrespetado
dialog.matches('Agresividad', [function (session, args) {session.endConversation("😳Esa no es una respuesta apropiada para alguien de tu edad");}]);
// Cambio de Nombre
dialog.matches('CambioNombre', [function (session, args) { session.beginDialog('CambioNombre', {intent: args.intent, entities: args.entities});}]);
// Funcionalidad PLUS: Noticias TOP de (Negocios, Tecnologia, Entretenimiento)
dialog.matches('Noticias', [function (session, args) { session.beginDialog('Noticias', {intent: args.intent, entities: args.entities});}]);
// Funcionalidad PLUS: Clima en la universidad
dialog.matches('Clima', [function (session, args) { session.beginDialog('Clima', {intent: args.intent, entities: args.entities});}]);
// Match para abortar
dialog.matches('Salida', [
    function (session, args) { 
        var score = args.score;
        if (score > 0){
            session.send("😊 Ok!, aquí estaré cuando me necesites. Escribe 'Menu' y te mostraré los servicios en que puedo ayudarte.");
            builder.Prompts.choice(session, "Ayúdame a mejorar 😊, califica mi servicio de 1 a 5 ✅, siendo 5 la calificación más alta: ", '1|2|3|4|5', { listStyle: builder.ListStyle.button });
        } else{session.endDialog('Default');}
    },

    /**
	 * Solicitar calificacion de servicio
	 */
	function(session, results, next) {
        var calificacion = results.response.entity;
        session.endConversation("Calificación guardada, gracias 😁")
	},

]);

// Match para None
//dialog.matches('None', [function (session, args) { 
    //session.endConversation("😨 No te entiendo, para ver el menu en lo que te puedo ayudar escribe 'HOLA'.");
//    session.beginDialog('Default');
//}]);

// Dialogos con QnA
dialog.matches('qna', [
    function (session, args, next) {
		// KSSPSCORE:
		// Hacer peticion de uso con metrica FAQS
        var inputUser = session.message.text;
        var lengthInputUser = inputUser.match(/\w+/g).length;
        var answerEntity = builder.EntityRecognizer.findEntity(args.entities, 'answer');
        // OJO KSSP preguntas con mas de 3 palabras para mejorar precision de estas
        // 20180426 Se cambia ahora como criterio el Score (lengthInputUser >= 3)
        if (answerEntity.score >= 0.50) {
            session.send(answerEntity.entity);
        }else{
            session.beginDialog('Default');
        }
    }
]);

///DIALOGOS no principales
//parámetros para los diálogos
var parametros={
	_builder:builder,
	_ecibot:ecibot,
	_request:request
}

//Pide el nivel de grado y lo retorna al diálogo anterior que haya sido llamado
var dialogo = require('./dialog/pedirNivelGrado');
bot.dialog('pedirNivelGrado', dialogo.getDialogo(parametros));

//pide la información que el usuario necesita saber sobre admisiones y la retorna al diálogo que lo llamó
dialogo = require('./dialog/Admisiones/pedirInfoGuiaAdmisiones');
bot.dialog('pedirInfoGuiaAdmisiones', dialogo.getDialogo(parametros));

//pregunta si el usuario es extrangero y lo retorna
dialogo = require('./dialog/preguntarSiEsExtranjero');
bot.dialog('preguntarSiEsExtranjero', dialogo.getDialogo(parametros));

//dialogo que da información de documentos según el nivel de grado
dialogo = require('./dialog/Admisiones/guiaAdmisionesDocumentos');
bot.dialog('guiaAdmisionesDocumentos', dialogo.getDialogo(parametros));

//dialogo que sostiene la conversación principal sobre Admisiones
dialogo = require('./dialog/Admisiones/guiaAdmisiones');
bot.dialog('guiaAdmisiones', dialogo.getDialogo(parametros));

//dialogo que muestra los requisitos de Admisiones
dialogo = require('./dialog/Admisiones/guiaAdmisionesRequisitos');
bot.dialog('guiaAdmisionesRequisitos', dialogo.getDialogo(parametros));

//ayuda al usuario a saber cómo está el proceso de matrícula
dialogo = require('./dialog/Admisiones/guiaAdmisionesRespuesta');
bot.dialog('guiaAdmisionesRespuesta', dialogo.getDialogo(parametros));

//muestra los pasos para admisión y su el usuario quiere se inicia el paso a paso para que inicie el proces de admisión
dialogo = require('./dialog/Admisiones/guiaAdmisionesPasos');
bot.dialog('guiaAdmisionesPasos', dialogo.getDialogo(parametros));